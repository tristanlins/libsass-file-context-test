#include <stdio.h>
#include <sass.h>
#include <sass2scss.h>
#include <string.h>
#include <stdlib.h>

int main() {
    printf("create compiler\n");
    struct Sass_File_Context *sass_context = sass_make_file_context("../sass/style.scss");
    struct Sass_Options *sass_options = sass_file_context_get_options(sass_context);
    sass_option_set_output_path(sass_options, "../css/style.css");
    struct Sass_Compiler *sass_compiler = sass_make_file_compiler(sass_context);

    printf("..parse\n");
    sass_compiler_parse(sass_compiler);

    printf("..execute\n");
    sass_compiler_execute(sass_compiler);

    const char *c_css = sass_context_get_output_string(sass_context);
    const char *c_source_map = sass_context_get_source_map_string(sass_context);
    int error_status = sass_context_get_error_status(sass_context);
    const char *c_error_json = sass_context_get_error_json(sass_context);
    const char *c_error_text = sass_context_get_error_text(sass_context);
    const char *c_error_message = sass_context_get_error_message(sass_context);
    const char *c_error_file = sass_context_get_error_file(sass_context);
    const char *c_error_src = sass_context_get_error_src(sass_context);

    printf("css: %s\n", c_css);
    printf("source map: %s\n", c_source_map);
    printf("error_status: %d\n", error_status);
    printf("error_json: %s\n", c_error_json);
    printf("error_text: %s\n", c_error_text);
    printf("error_message: %s\n", c_error_message);
    printf("error_file: %s\n", c_error_file);
    printf("error_src: %s\n", c_error_src);

    printf("..delete compiler\n");
    sass_delete_compiler(sass_compiler);
    printf("..delete context\n");
    sass_delete_file_context(sass_context);

	return 0;
}
