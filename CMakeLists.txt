cmake_minimum_required(VERSION 3.2)
project(compile)

include_directories(
    "libsass/include"
)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

message("search for libsass static library in: ${CMAKE_SOURCE_DIR}/libsass/lib")
find_library(SASS_EXT_LIBRARY NAMES sass libsass PATHS "${CMAKE_SOURCE_DIR}/libsass/lib" NO_DEFAULT_PATH)
message(STATUS "libsass library: ${SASS_EXT_LIBRARY}")
add_library(sass UNKNOWN IMPORTED)
set_property(TARGET sass PROPERTY IMPORTED_LOCATION "${SASS_EXT_LIBRARY}")

file(WRITE empty.cpp "")

add_executable(compile compile.c empty.cpp)
set_property(TARGET compile PROPERTY C_STANDARD 99)

set(LINK_FLAGS ${LINK_FLAGS} "-Wl,-V,--whole-archive,-ldl")

target_link_libraries(compile -static-libgcc -static-libstdc++ "-Wl,-V,--whole-archive,-ldl" sass "-Wl,-V,--no-whole-archive,-ldl")

